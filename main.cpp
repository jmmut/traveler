#include <iostream>
#include <limits>
#include <vector>
#include <string>
#include <set>
#include "SignalHandler.h"
#include <stdexcept>

using namespace std;

struct Path {
    vector<int> nodes;
    double distance;
};

// debug counts
int calls = 0;
int base = 0;
int recursion = 0;
int deep = 0;

Path shortestPath(const vector<vector<double>> &matrix, set<int> &available, Path &current) {
    deep++;
//    cout << "\r" << deep;
//    cout << deep << " : ";
//    for (int j = 0; j < deep; ++j) {
//        cout << "*";
//    }
//    cout << endl;
    Path next;
    Path candidate;

    calls++;
    next.distance = numeric_limits<double>::infinity();

    if (available.size() == 0) {
        current.distance = 0;
        for (int i = 0; i < current.nodes.size(); ++i) {
            current.distance += matrix[current.nodes[i]][current.nodes[(i + 1) % current.nodes.size()]];
        }
        next = current;
        base++;
    } else {
        set<int> copyAvailable;
        copyAvailable.insert(available.begin(), available.end());
        for (auto item : available) {
            current.nodes.push_back(item);
            copyAvailable.erase(item);
            recursion++;
            candidate = shortestPath(matrix, copyAvailable, current);
            if (candidate.distance < next.distance) {
                next = candidate;
            }
            copyAvailable.insert(item);
            current.nodes.pop_back();
        }
    }

    deep--;
    return next;
}




int main(int argc, char **argv) {
    SigsegvPrinter::activate(cout);


    if (argc != 2) {
        cout << "give me a number of nodes" << endl;
        return 1;
    }
    int num = stoi(argv[1]);


    vector<vector<double>> matrix;
    matrix.resize(num);
    for (int k = 0; k < num; ++k) {
        matrix[k].resize(num);
    }


    int i, j;
    string read;
    cout << "reading from stdin the distances matrix..." << endl;
    for (i = 0; i < num; i++) {
        for (j = 0; j < num; ++j) {
            cin >> read;
            try {
                matrix[i][j] = stod(read);
            } catch (exception parse) {
                invalid_argument e(string("unexpected value, need doubles: ") + parse.what());
                throw e;
            }
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }

    set<int> available;
    for (int l = 0; l < num; ++l) {
        available.insert(l);
    }
    Path current;

    Path shortest = shortestPath(matrix, available, current);

    cout << endl;
    cout << "calls = " << calls << endl;
    cout << "base = " << base << endl;
    cout << "recursion = " << recursion << endl;
    cout << "shortest path: " << shortest.distance << " [ ";
    for (auto node : shortest.nodes) {
        cout << node << " ";
    }
    cout << "]" << endl;
    return 0;
}
