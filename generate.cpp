#include <iostream>
#include <limits>
#include <vector>
#include <random>
#include <assert.h>

using namespace std;

int main(int argc, char **argv) {

    if (argc != 2) {
        cout << "give me a number of nodes" << endl;
        return 1;
    }
    int num = stoi(argv[1]);
    vector<vector<double>> matrix;

    matrix.resize(num);
    for (int k = 0; k < num; ++k) {
        matrix[k].resize(num);
    }
    random_device rd;
    default_random_engine generator(rd());
    uniform_real_distribution<double> distribution(1, 1000000);

    int j;
    int i;
    for (i = 0; i < num; i++) {
        for (j = 0; j < i; ++j) {
            matrix[i][j] = distribution(generator);
            matrix[j][i] = matrix[i][j];
        }
        assert(i == j);
        matrix[i][j] = numeric_limits<double>::infinity();
    }

    for (i = 0; i < num; i++) {
        for (j = 0; j < num; ++j) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }

    return 0;
}


