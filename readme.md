# Brute Force Traveler Problem Solver

This program is used to test against other algorithms to check optimal solution.

To run:

```
cmake -G"Unix Makefiles"
cmake --build .
./generate 4 > matrix.txt
./traveler 4 < matrix.txt
```

`traveler` receives a number of nodes in the graph, and reads from standard input the distances matrix.

The matrix is assumed to be `doubles`. Note that `inf` is a [valid](http://en.cppreference.com/w/cpp/string/basic_string/stof) double. Use inf for the distance of a node with itself.

Current version is not efficient, does not skip branches with distances already longer than a previous solution.

