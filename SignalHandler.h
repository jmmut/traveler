#ifndef __SIGNALHANDLER_H_
#define __SIGNALHANDLER_H_
#include <stdexcept>
#include <cxxabi.h>
using std::runtime_error;

class SignalException : public runtime_error
{
public:
   SignalException(const std::string& _message)
      : std::runtime_error(_message)
   {}
};

class SignalHandler
{
protected:
    static bool mbGotExitSignal;

public:
    SignalHandler();
    ~SignalHandler();

    static bool gotExitSignal();
    static void setExitSignal(bool _bExitSignal);

    void        setupSignalHandlers();
    static void exitSignalHandler(int _ignored);

};

class SigsegvPrinter {
private:
    static std::ostream *out;
    static void handleSignal(int ignored);
public:
    static void activate(std::ostream& os);
};
//static inline
void print_stacktrace(std::ostream &out, unsigned int max_frames = 63);
//void ActivateSigsegvPrinter(std::ostream & os);
//void handleSignal();

#endif
          
