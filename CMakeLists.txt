cmake_minimum_required(VERSION 2.8)
project(traveler)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

add_library(Signal SignalHandler.cpp)
set(SOURCE_FILES
    main.cpp
    SignalHandler.h)

add_executable(traveler ${SOURCE_FILES})
target_link_libraries(traveler Signal)
